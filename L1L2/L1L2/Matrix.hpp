#ifndef MATRIX_H
#define MATRIX_H

#include "defines.h"

template<typename T>
class Matrix
{
private:

	// Access to row of the matrix
	class RowProxy
	{
	private:
		T* const row;

	public:
		const uint cols;

		RowProxy(T* row_, uint cols_) : row(row_), cols(cols_) { }

		T& operator[](uint i) { return row[i]; }
	};

private:
	T* data;
	bool isSlice = false;

	// Slice constructor
	Matrix(T* base, uint r, uint c, uint C)
		: rows(r), cols(c), C(C), size(r* c)
	{
		isSlice = true;
		data = base;
	}

public:
	const uint rows;
	const uint cols;
	const uint C;
	const uint size;

	Matrix(uint r, uint c)
		: rows(r), cols(c), C(c), size(r* c)
	{
		data = new T[size]();
	}
	Matrix(const Matrix<T>& other)
		: rows(other.rows), cols(other.cols), C(other.C), size(other.size)
	{
		if (other.isSlice)
		{
			data = other.data;
		}
		else
		{
			data = new T[size];
			memcpy(data, other.data, size * sizeof(T));
		}
	}

	~Matrix()
	{
		if (!isSlice)
			delete[] data;
	}

	Matrix slice(tuple origin, tuple size) const
	{
		return Matrix<T>(data + (origin.i * C) + origin.j, size.i, size.j, C);
	}

	void set(T v)
	{
		if (isSlice)
		{
			for (uint i = 0; i < rows; i++)
				for (uint j = 0; j < cols; j++)
					(*this)[i][j] = v;
		}
		else
		{
			for (uint i = 0; i < size; i++)
				data[i] = v;
		}
	}

	void clear()
	{
		if (isSlice)
		{
			for (uint i = 0; i < rows; i++)
				for (uint j = 0; j < cols; j++)
					(*this)[i][j] = 0;
		}
		else
			memset(data, 0, size * sizeof(T));
	}

	void print(const char* name = nullptr)
	{
		if (name)
			printf("\n%s = \n", name);

		for (uint i = 0; i < rows; i++)
		{
			for (uint j = 0; j < cols; j++)
				printf("%d ", (*this)[i][j]);

			printf("\n");
		}

		printf("\n");
	}

	RowProxy operator[](uint i) const
	{
		return RowProxy(data + (i * C), cols);
	}
	Matrix<T> operator()(uint way) const
	{
		uint rhalf = rows / 2;
		uint chalf = cols / 2;

		switch (way)
		{
		case 11: return slice({ 0,     0 }, { rhalf, chalf });
		case 12: return slice({ 0,     chalf }, { rhalf, chalf });
		case 21: return slice({ rhalf, 0 }, { rhalf, chalf });
		case 22: return slice({ rhalf, chalf }, { rhalf, chalf });

		default: break;
		}

		return slice({ 0, 0 }, { rows, cols });
	}

	bool operator==(const Matrix<T>& other) const
	{
		if (cols != other.cols || rows != other.rows)
			return false;	// Dimensions do not match

		if (isSlice || other.isSlice)
		{
			if (data == other.data)	 // Pointing to same address
				return true;

			for (uint i = 0; i < rows; i++)
				for (uint j = 0; j < cols; j++)

					if ((*this)[i][j] != other[i][j])
						return false;
		}
		else
		{
			for (uint i = 0; i < size; i++)
				if (data[i] != other.data[i])
					return false;	// Element does not match
		}

		return true;
	}
	bool operator!=(const Matrix<T>& other) const
	{
		return !(*this == other);
	}
};

#endif // MATRIX_H
