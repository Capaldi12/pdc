#ifndef MULTIPLICATION_H
#define MULTIPLICATION_H

#include <thread>
#include <atomic>
#include <mutex>

#include <omp.h>

#include "defines.h"
#include "Matrix.hpp"

using std::thread;
using std::atomic;
using std::mutex;

// Calculate value of matrix element for multiplication
template<typename T>
void calculateCell(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B, uint x, uint y)
{
    uint sum = 0;

    for (uint i = 0; i < A.cols; i++)
        sum += A[x][i] * B[i][y];

    C[x][y] = sum;
}

// Calculate range of matrix elements for multiplication
template<typename T>
void calculateRange(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B, uint start, uint stop)
{
    for (uint i = start; i < stop; i++)
        calculateCell(C, A, B, i / C.cols, i % C.cols);
}

// Sequential matrix multiplication
template<typename T>
void multiplySequential(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    for (uint i = 0; i < C.rows; i++)
        for (uint j = 0; j < C.cols; j++)
            calculateCell(C, A, B, i, j);
}

// Matrix multiplication using specified number of threads (including master thread)
template<typename T>
void multiplyThreaded(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B, uint num_threads = 4)
{
    if (num_threads > MAX_THREAD)
        num_threads = MAX_THREAD;

    std::thread threads[MAX_THREAD - 1];

    uint step = C.size / num_threads;
    uint start = 0;


    for (uint i = 0; i < num_threads - 1; i++)
    {
        threads[i] = std::thread(
            [&, start, step]()
            {
                calculateRange(C, A, B, start, start + step);
            }
        );

        start += step;
    }

    calculateRange(C, A, B, start, C.size);

    for (uint i = 0; i < num_threads - 1; i++)
        threads[i].join();
}


// Calculate value of matrix element for multiplication and add it to the destination
template<typename T>
void calculateAndAddCell(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B, uint x, uint y)
{
    uint sum = 0;

    for (uint i = 0; i < A.cols; i++)
        sum += A[x][i] * B[i][y];

    C[x][y] += sum;
}

// Sequential matrix multiplication (add to destination instead of replace)
template<typename T>
void multiplyAndAddSequential(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    for (uint i = 0; i < C.rows; i++)
        for (uint j = 0; j < C.cols; j++)
            calculateAndAddCell(C, A, B, i, j);
}

// Matrix multiplication using Divide and �onquer algorithm (has race condition)
template<typename T>
void divideAndConquer(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    thread threads[8];

    // Parallel multiplication of blocks
    threads[0] = thread([&] { multiplyAndAddSequential(C(11), A(11), B(11)); });
    threads[1] = thread([&] { multiplyAndAddSequential(C(11), A(12), B(21)); });
    threads[2] = thread([&] { multiplyAndAddSequential(C(12), A(11), B(12)); });
    threads[3] = thread([&] { multiplyAndAddSequential(C(12), A(12), B(22)); });
    threads[4] = thread([&] { multiplyAndAddSequential(C(21), A(21), B(11)); });
    threads[5] = thread([&] { multiplyAndAddSequential(C(21), A(22), B(21)); });
    threads[6] = thread([&] { multiplyAndAddSequential(C(22), A(21), B(12)); });
    threads[7] = thread([&] { multiplyAndAddSequential(C(22), A(22), B(22)); });

    for (auto& thread : threads) thread.join();
}


// Matrix elementwise sum
template<typename T>
void addSequential(const Matrix<T>& D, const Matrix<T>& S)
{
    for (uint i = 0; i < D.rows; i++)
        for (uint j = 0; j < D.cols; j++)
            D[i][j] += S[i][j];
}

// Divide and Conquer algorithm using separate memory for each thread
template<typename T>
void divideAndConquer_separateMemory(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    thread threads[8];

    Matrix<T> M(C.rows, C.cols);

    // Split multiplication between C and M
    threads[0] = thread([&] { multiplySequential(C(11), A(11), B(11)); });
    threads[1] = thread([&] { multiplySequential(C(12), A(11), B(12)); });
    threads[2] = thread([&] { multiplySequential(C(21), A(21), B(11)); });
    threads[3] = thread([&] { multiplySequential(C(22), A(21), B(12)); });
    
    threads[4] = thread([&] { multiplySequential(M(11), A(12), B(21)); });
    threads[5] = thread([&] { multiplySequential(M(12), A(12), B(22)); });
    threads[6] = thread([&] { multiplySequential(M(21), A(22), B(21)); });
    threads[7] = thread([&] { multiplySequential(M(22), A(22), B(22)); });

    for (uint i = 0; i < 8; i++) threads[i].join();

    // Sum the results
    threads[0] = thread([&] { addSequential(C(11), M(11)); });
    threads[1] = thread([&] { addSequential(C(12), M(12)); });
    threads[2] = thread([&] { addSequential(C(21), M(21)); });
    threads[3] = thread([&] { addSequential(C(22), M(22)); });

    for (uint i = 0; i < 4; i++) threads[i].join();
}


// Calculate value of matrix element for multiplication and add it to the destination (using atomic types as destination)
template<typename T>
void calculateAndAddCell_atomic(const Matrix<atomic<T>>& C, const Matrix<T>& A, const Matrix<T>& B, uint x, uint y)
{
    uint sum = 0;

    for (uint i = 0; i < A.cols; i++)
        sum += A[x][i] * B[i][y];

    C[x][y] += sum;
}

// Sequential matrix multiplication (add to destination instead of replace) (using atomic types as destination)
template<typename T>
void multiplyAndAddSequential_atomic(const Matrix<atomic<T>>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    for (uint i = 0; i < C.rows; i++)
        for (uint j = 0; j < C.cols; j++)
            calculateAndAddCell_atomic(C, A, B, i, j);
}

// Divide and Conquer algorithm using atomic types as destination
template<typename T>
void divideAndConquer_atomic(const Matrix<atomic<T>>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    thread threads[8];

    // Parallel multiplication of blocks
    threads[0] = thread([&] { multiplyAndAddSequential_atomic(C(11), A(11), B(11)); });
    threads[1] = thread([&] { multiplyAndAddSequential_atomic(C(11), A(12), B(21)); });
    threads[2] = thread([&] { multiplyAndAddSequential_atomic(C(12), A(11), B(12)); });
    threads[3] = thread([&] { multiplyAndAddSequential_atomic(C(12), A(12), B(22)); });
    threads[4] = thread([&] { multiplyAndAddSequential_atomic(C(21), A(21), B(11)); });
    threads[5] = thread([&] { multiplyAndAddSequential_atomic(C(21), A(22), B(21)); });
    threads[6] = thread([&] { multiplyAndAddSequential_atomic(C(22), A(21), B(12)); });
    threads[7] = thread([&] { multiplyAndAddSequential_atomic(C(22), A(22), B(22)); });

    for (auto& thread : threads) thread.join();
}

// Mutex for critical section
mutex mx;

// Calculate value of matrix element for multiplication and add it to the destination (using mutex for syncronization)
template<typename T>
void calculateAndAddCell_critical(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B, uint x, uint y)
{
    uint sum = 0;

    for (uint i = 0; i < A.cols; i++)
        sum += A[x][i] * B[i][y];

    mx.lock();

    C[x][y] += sum;

    mx.unlock();
}

// Sequential matrix multiplication (add to destination instead of replace) (using mutex for syncronization)
template<typename T>
void multiplyAndAddSequential_critical(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    for (uint i = 0; i < C.rows; i++)
        for (uint j = 0; j < C.cols; j++)
            calculateAndAddCell_critical(C, A, B, i, j);
}

// Divide and Conquer algorithm using mutex for syncronization
template<typename T>
void divideAndConquer_critical(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    thread threads[8];

    // Parallel multiplication of blocks
    threads[0] = thread([&] { multiplyAndAddSequential_critical(C(11), A(11), B(11)); });
    threads[1] = thread([&] { multiplyAndAddSequential_critical(C(11), A(12), B(21)); });
    threads[2] = thread([&] { multiplyAndAddSequential_critical(C(12), A(11), B(12)); });
    threads[3] = thread([&] { multiplyAndAddSequential_critical(C(12), A(12), B(22)); });
    threads[4] = thread([&] { multiplyAndAddSequential_critical(C(21), A(21), B(11)); });
    threads[5] = thread([&] { multiplyAndAddSequential_critical(C(21), A(22), B(21)); });
    threads[6] = thread([&] { multiplyAndAddSequential_critical(C(22), A(21), B(12)); });
    threads[7] = thread([&] { multiplyAndAddSequential_critical(C(22), A(22), B(22)); });

    for (auto& thread : threads) thread.join();
}


// Iterative multiplication using OpenMP for parallel execution
template<typename T>
void multiplyOpenMP(const Matrix<T>& C, const Matrix<T>& A, const Matrix<T>& B)
{
    #pragma omp parallel for
    for (int i = 0; i < C.size; i++)
        calculateCell(C, A, B, i / C.cols, i % C.cols);
}
#endif // !MULTIPLICATION_H
