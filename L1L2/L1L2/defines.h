#ifndef DEFINES_H
#define DEFINES_H

using uint = unsigned int;
struct tuple { uint i, j; };

constexpr uint MAX_THREAD = 16;

#endif // !DEFINES_H
