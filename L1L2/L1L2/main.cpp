#include <stdio.h>
#include <iostream>
#include <string>

#include <chrono>
#include <random>

#include "defines.h"
#include "Matrix.hpp"
#include "Multiplication.hpp"

using hrc = std::chrono::high_resolution_clock;
using ns = std::chrono::nanoseconds;

#define lambda [&]

// Measures execution time of passed lambda
// Result is in nanoseconds
auto timeit(auto& f) 
{
    auto start = hrc::now();

    f();  // I don't know how, but this works

    auto end = hrc::now();

    return end - start;
}

#define red     "\x1B[91m"
#define green   "\x1B[92m"
#define yellow  "\x1B[93m"
#define blue    "\x1B[94m"
#define magenta "\x1B[95m"
#define cyan    "\x1B[96m"
#define reset   "\033[0m"

void usage(const char* name)
{
    printf(
        "Usage:\n"
        yellow "  %s" green " compare" blue " <size> <threads>" reset " - compare algorithms on matrices of given size.\n"
        yellow "  %s" green " benchmark" cyan " <alg>" blue " <size> [<threads>]" reset " - time execution of given algorithm.\n"
        "  Algorithms:\n"
        cyan "    single"   reset " - Singlethreaded algorithm.\n"
        cyan "    multi"    reset " - Multithreaded version of singlethreaded algorithm. " blue "`threads`" reset " - number of threads.\n"
        cyan "    divide"   reset " - Divide and Conquer algorithm.\n"
        cyan "    memory"   reset " - Divide and Conquer algorithm with separate memory.\n"
        cyan "    atomic"   reset " - Divide and Conquer algorithm with atomic operations.\n"
        cyan "    critical" reset " - Divide and Conquer algorithm with critical section.\n"
        cyan "    omp"      reset " - Parallel version of singlethreaded algorithm using OpenMP. " blue "`threads`" reset " - number of threads.\n\n"
        "  For Divide and Conquer algorithms to work " blue "`size`" reset " must be even.\n\n",
        name, name
    );
}

// Init rng
static std::random_device rd;
static std::mt19937 random(rd());

void mock(Matrix<uint>& M, uint max_val = 256)
{
    for (uint i = 0; i < M.rows; i++)
        for (uint j = 0; j < M.cols; j++)
            M[i][j] = random() % max_val;
}

template<typename T>
bool equals_atomic(Matrix<T>& M, Matrix<atomic<T>>& O)
{
    if (M.cols != O.cols || M.rows != O.rows)
        return false;	// Dimensions do not match

    for (uint i = 0; i < M.rows; i++)
        for (uint j = 0; j < M.cols; j++)
            if (M[i][j] != O[i][j])
                return false;

    return true;
}

enum class Mode : uint {
    single, 
    multi, 
    omp,
    divide, 
    memory, 
    atomic, 
    critical,
};

using std::string;

int main(int argc, const char* argv[])
{
    if (argc <= 1)
    {
        usage(argv[0]);
        return 1;
    }

    string arg1 = argv[1];
    
    if (arg1 == "compare")
    {
        if (argc < 4)
        {
            fprintf(stderr, red "Not enough arguments for compare mode!\n"
                   "Please provide matrix size and number of threads.\n\n" reset);
            usage(argv[0]);
            return 1;
        }

        int size_int;
        uint num_threads;

        if (sscanf_s(argv[2], "%d", &size_int) != 1)
        {
            fprintf(stderr, red "Cannot parse matrix size: '%s'!\n\n" reset, argv[2]);
            return 1;
        }

        if (size_int < 2)
        {
            fprintf(stderr, red "Matrix size is too small!\n\n" reset);
            return 1;
        }

        if (size_int % 2)
        {
            fprintf(stderr, red "Matrix size must be even!\n\n" reset);
            return 1;
        }

        uint size = (uint)size_int;

        if (sscanf_s(argv[3], "%u", &num_threads) != 1)
        {
            fprintf(stderr, red "Cannot parse number of threads: '%s'!\n\n" reset, argv[3]);
            return 1;
        }

        if (num_threads > MAX_THREAD)
        {
            num_threads = MAX_THREAD;
            fprintf(stderr, "Number of threads is too big. Defaulting to 16.\n");
        }

        uint omp_threads = num_threads;
        uint max_threads = omp_get_num_procs();

        if (omp_threads > max_threads)
        {
            omp_threads = max_threads;
            fprintf(stderr, "Number of threads exceeds number of logical processors. Defaulting to %u.\n", max_threads);
        }

        omp_set_num_threads(omp_threads);

        try
        {
            Matrix<uint>
                A(size, size),
                B(size, size),

                C_seq(size, size),
                C_thr(size, size),
                C_dnc(size, size),

                C_mem(size, size),
                C_mtx(size, size),
                C_omp(size, size);
                

            Matrix<atomic<uint>> 
                C_atm(size, size);

            mock(A); mock(B);

            auto seq = lambda{ multiplySequential(C_seq, A, B); };
            auto thr = lambda{ multiplyThreaded(C_thr, A, B, num_threads); };
            auto dnc = lambda{ divideAndConquer(C_dnc, A, B); };

            auto mem = lambda{ divideAndConquer_separateMemory(C_mem, A, B); };
            auto atm = lambda{ divideAndConquer_atomic(C_atm, A, B); };
            auto mtx = lambda{ divideAndConquer_critical(C_mtx, A, B); };

            auto omp = lambda{ multiplyOpenMP(C_omp, A, B); };

            printf("\nComparing algorithms on size = " blue "%u\n\n" reset, size);

            printf("Sequential calculation took " cyan "%.3f s.\n" reset, timeit(seq).count() / 1e9);
            printf("Multihreaded calculation (" blue "%u" reset " threads) took " cyan "%.3f s.\n" reset, 
                num_threads, timeit(thr).count() / 1e9);
            printf("Divide and Conquer calculation took " cyan "%.3f s.\n\n" reset, timeit(dnc).count() / 1e9);

            printf("Divide and Conquer with separate memory calculation took " cyan "%.3f s.\n" reset, timeit(mem).count() / 1e9);
            printf("Divide and Conquer with atomics calculation took " cyan "%.3f s.\n" reset, timeit(atm).count() / 1e9);
            printf("Divide and Conquer with critical section calculation took " cyan "%.3f s.\n\n" reset, timeit(mtx).count() / 1e9);
            
            printf("OpenMP parallel calculation (" blue "%u" reset " threads) took " cyan "%.3f s.\n\n" reset, 
                omp_threads, timeit(omp).count() / 1e9);

            if (C_seq == C_thr)
                printf(green "Multithreaded calculation is correct.\n" reset);
            else
                printf(red "Multithreaded calculation is wrong!\n" reset);

            if (C_seq == C_dnc)
                printf(green "Divide and Conquer calculation is correct.\n\n" reset);
            else
                printf(red "Divide and Conquer calculation is wrong!\n\n" reset);

            if (C_seq == C_mem)
                printf(green "Divide and Conquer with separate memory calculation is correct.\n" reset);
            else
                printf(red "Divide and Conquer with separate memory calculation is wrong!\n" reset);

            if (equals_atomic(C_seq, C_atm))
                printf(green "Divide and Conquer with atomics calculation is correct.\n" reset);
            else
                printf(red "Divide and Conquer with atomics calculation is wrong!\n" reset);

            if (C_seq == C_mtx)
                printf(green "Divide and Conquer with critical section calculation is correct.\n\n" reset);
            else
                printf(red "Divide and Conquer with critical section calculation is wrong!\n\n" reset);
            
            if (C_seq == C_omp)
                printf(green "OpenMP parallel calculation is correct.\n\n" reset);
            else
                printf(red "OpenMP parallel calculation is wrong!\n\n" reset);
                
            return 0;
        }
        catch (std::bad_alloc)
        {
            fprintf(stderr, red "Out of memory. Try smaller matrices.\n\n" reset);
            return 1;
        }
    }

    if (arg1 == "benchmark")
    {
        if (argc < 4)
        {
            fprintf(stderr, red "Not enough arguments for benchmark mode!\n\n" reset);
            usage(argv[0]);
            return 1;
        }

        Mode mode;
        string arg2 = argv[2];

        if      (arg2 == "single")   mode = Mode::single;
        else if (arg2 == "multi")    mode = Mode::multi;
        else if (arg2 == "omp")      mode = Mode::omp;
        else if (arg2 == "divide")   mode = Mode::divide;
        else if (arg2 == "memory")   mode = Mode::memory;
        else if (arg2 == "atomic")   mode = Mode::atomic;
        else if (arg2 == "critical") mode = Mode::critical;
        else
        {
            fprintf(stderr, red "Unknown algorithm: '%s'!\n\n" reset, arg2.c_str());
            usage(argv[0]);
            return 1;
        }
        
        int size_int;

        if (sscanf_s(argv[3], "%u", &size_int) != 1)
        {
            fprintf(stderr, red "Cannot parse matrix size: '%s'!\n\n" reset, argv[3]);
            return 1;
        }

        if (size_int < 2)
        {
            fprintf(stderr, red "Matrix size is too small!\n\n" reset);
            return 1;
        }

        if (mode >= Mode::divide)
        {
            if (size_int % 2)
            {
                fprintf(stderr, red "Matrix size must be even!\n\n" reset);
                return 1;
            }
        }

        uint size = (uint)size_int;

        uint num_threads = 1;

        if (mode == Mode::multi || mode == Mode::omp)
        {
            if (argc < 5)
            {
                fprintf(stderr, red "Please provide number of threads!\n\n" reset);
                usage(argv[0]);
                return 1;
            }
            else
            {
                if (sscanf_s(argv[4], "%u", &num_threads) != 1)
                {
                    fprintf(stderr, red "Cannot parse number of threads: '%s'!\n\n" reset, argv[3]);
                    return 1;
                }

                if (mode == Mode::multi)
                    if (num_threads > MAX_THREAD)
                    {
                        num_threads = MAX_THREAD;
                        fprintf(stderr, "Number of threads is too big. Defaulting to 16.\n");
                    }

                if (mode == Mode::omp)
                {
                    uint max_threads = omp_get_num_procs();

                    if (mode == Mode::omp && num_threads > max_threads)
                    {
                        num_threads = max_threads;
                        fprintf(stderr, "Number of threads exceeds number of logical processors. Defaulting to %u.\n", max_threads);
                    }

                    omp_set_num_threads(num_threads);
                }
            }
        }

        try
        {
            Matrix<uint>
                // Input matrices (random-filled)
                A(size, size),
                B(size, size),

                // Output matrices (empty)
                C(size, size);

            Matrix<atomic<uint>> C_atm(size, size);

            mock(A); mock(B);


            // For some reason I need to save lambda to the variable before I can pass it to timeit
            auto sequential = lambda{ multiplySequential(C, A, B); };
            auto threaded = lambda{ multiplyThreaded(C, A, B, num_threads); };
            auto divide = lambda{ divideAndConquer(C, A, B); };

            auto memory = lambda{ divideAndConquer_separateMemory(C, A, B); };
            auto atomics = lambda{ divideAndConquer_atomic(C_atm, A, B); };
            auto critical = lambda{ divideAndConquer_critical(C, A, B); };

            auto omp = lambda{ multiplyOpenMP(C, A, B); };

            ns elapsed;

            // Select which one to do
            if (mode == Mode::single)
                elapsed = timeit(sequential);
            else
            {
                if (mode == Mode::multi)
                    elapsed = timeit(threaded);

                else if (mode == Mode::omp)
                    elapsed = timeit(omp);

                else if (mode == Mode::divide)
                    elapsed = timeit(divide);

                else if (mode == Mode::memory)
                    elapsed = timeit(memory);

                else if (mode == Mode::atomic)
                    elapsed = timeit(atomics);

                else
                    elapsed = timeit(critical);

                // Compare to sequential
                Matrix<uint> R(size, size);

                multiplySequential(R, A, B);

                if (mode == Mode::atomic)
                {
                    if (!equals_atomic(R, C_atm))
                        std::cerr << "not equals" << std::endl;
                }
                else if (C != R)
                    std::cerr << "not equals" << std::endl;
            }

            std::cout << elapsed.count() << std::endl;
        }
        catch (std::bad_alloc)
        {
            std::cerr << "out of memory" << std::endl;
        }

        return 0;
    }

    fprintf(stderr, red "Unknown mode: '%s'!\n\n" reset, arg1.c_str());
    usage(argv[0]);
    return 1;
}
