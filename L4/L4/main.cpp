#include <stdio.h>
#include <random>
#include <chrono>
#include <string>

#include <omp.h>

// Swap values im pointers
inline void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

// Recursive quicksort implementation
void _quicksort(int *begin, int *end)
{
	if (begin >= end) return;

	int *middle = begin + (end - begin) / 2;

	// Select optimal pivot
	if (*middle < *begin) swap(begin, middle);
	if (*end    < *begin) swap(begin, end);
	if (*middle < *end)   swap(middle, end);

	int pivot = *end;

	int *i = begin - 1;
	int *j = end + 1;

	// Rearange array
	while (true)
	{
		do i += 1; while (*i < pivot);
		do j -= 1; while (*j > pivot);

		if (i >= j) break;

		swap(i, j);
	}

    #pragma omp task
	_quicksort(begin, j);

	#pragma omp task
	_quicksort(j + 1, end);
}

// Convinience function
void quicksort(int *arr, int size)
{
	_quicksort(arr, arr + size - 1);
}

std::random_device rd;
std::mt19937 rng(rd());

// Generate random array
int* rand_arr(int size)
{
	int* a = new int[size];

	for (int i = 0; i < size; i++) 
		
		a[i] = rng();

	return a;
}

// Print array (for testing)
void print_arr(int* a, int size)
{
	printf("[");

	for (int i = 0; i < size; i++)
	{
		if (i != 0) printf(", ");

		printf("%d", a[i]);
	}

	printf("]\n");
}

// Check if array is sorted
bool is_sorted(int* a, int size)
{
	for (int i = 1; i < size; i++)

		if (a[i - 1] > a[i])
			return false;

	return true;
}


using ns = std::chrono::nanoseconds;
using hrc = std::chrono::high_resolution_clock;
using std::chrono::duration_cast;


int main(int argc, char* argv[])
{
	int size;
	bool omp = false;
	bool console = false;

	if (argc >= 2)
	{
		console = true;
		sscanf_s(argv[1], "%d", &size);

		if (argc >= 3)
		{
			std::string arg2 = argv[2];

			if (arg2 == "--omp")
				omp = true;
			
			else
			{
				printf("Unknown flag %s", arg2.c_str());
				return 1;
			}
		}
	}
	else
	{
		printf("Enter array size:\n> ");
		scanf_s("%d", &size);

		printf("Use omp? [y/n]\n> ");

		char buf[2];

		scanf_s("%s", buf);

		if (buf[0] == 'y')
			omp = true;

		printf("\n");
	}

	int* a;

	try
	{
		a = rand_arr(size);
	}
	catch (std::bad_alloc e)
	{
		if (!console)
			printf("Array size is too big\n\n");
		else
			fprintf(stderr, "out of memory");

		return 1;
	}

	if (!console)
		printf("Sorting array of size %d\n", size);

	ns elapsed;

	auto start = hrc::now();

	if (omp)
	{
		#pragma omp parallel shared(a, size)
		{
			#pragma omp single
			quicksort(a, size);
		}

	}
	else
		quicksort(a, size);

	auto end = hrc::now();

	elapsed = end - start;
	
	if (!console)
		printf("Elapsed: %f ms\n", elapsed.count() / 10e6);
	else
		printf("%llu\n", elapsed.count());

	if (!console)
	{
		if (is_sorted(a, size))
			printf("Array is sorted\n\n");
		else
			printf("Array is not sorted\n\n");
	}

	return 0;
}